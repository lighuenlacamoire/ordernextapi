import { Controller, Get } from "@nestjs/common";
import { AppService } from "./app.service";
import { ApiOperation, ApiResponse, ApiTags, ApiQuery } from "@nestjs/swagger";

@ApiTags("example")
@Controller("example")
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiOperation({ summary: "Get example data" })
  @ApiResponse({ status: 200, description: "Success" })
  @ApiQuery({ name: "name", required: false, description: "Your name" })
  getHello(): string {
    return this.appService.getHello();
  }
}

import { Controller, Get } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags, ApiQuery } from "@nestjs/swagger";
import { PeopleService } from "./people.service";
import { PeopleDTO, StarWarsListDTO } from "src/models/DTO/StarWarsAggregators";

@ApiTags("people")
@Controller("people")
export class PeopleController {
  constructor(private readonly peopleService: PeopleService) {}

  @Get()
  @ApiOperation({ summary: "Get all people" })
  @ApiResponse({ status: 200, description: "Success" })
  @ApiQuery({})
  getAll(): Promise<StarWarsListDTO<PeopleDTO>> {
    return this.peopleService.getAll();
  }
}

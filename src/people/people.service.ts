import { HttpService } from "@nestjs/axios";
import { Injectable } from "@nestjs/common";
import { AxiosError, AxiosResponse } from "axios";
import { catchError, firstValueFrom, map } from "rxjs";
import {
  CharacterDTO,
  PlanetDTO,
  StarWarsListDTO,
  PeopleDTO,
} from "src/models/DTO/StarWarsAggregators";

@Injectable()
export class PeopleService {
  constructor(private readonly httpService: HttpService) {}
  getAll = async (): Promise<StarWarsListDTO<PeopleDTO>> => {
    const request = this.httpService
      .get<StarWarsListDTO<PeopleDTO>>("https://swapi.dev/api/people")
      .pipe(map((res) => res.data))
      .pipe(
        catchError((error: AxiosError) => {
          throw "An error happened!";
        }),
      );

    return await firstValueFrom(request);
  };
  findAll(): Promise<AxiosResponse<StarWarsListDTO<PeopleDTO>>> {
    return this.httpService.axiosRef.get("https://swapi.dev/api/people");
    //                      ^ AxiosInstance interface
  }
}

import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SwaggerInit } from "./swaggerExtension";
import { Logger } from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({ origin: "*" });
  app.setGlobalPrefix("api");

  SwaggerInit(app);

  await app.listen(process.env.PORT || 5001);
  const appUrl = await app.getUrl();
  Logger.log(`app is running on ${appUrl}`, "NestApplication");
}
bootstrap();

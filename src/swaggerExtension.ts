import { INestApplication } from "@nestjs/common";
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from "@nestjs/swagger";

/**
 * Swagger Configuration
 * @param app Nest App
 */
export function SwaggerInit(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle("NestJS API")
    .setDescription("App of NestJS")
    .setVersion("1.0")
    .addTag("API")
    .build();

  const options: SwaggerCustomOptions = {
    customSiteTitle: "NestJS Swagger",
  };
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("swagger", app, document, options);
}

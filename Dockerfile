FROM node:18 AS base

RUN npm install -g nodemon

WORKDIR /app
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./

RUN \
  if [ -f yarn.lock ]; then yarn install; \
  elif [ -f package-lock.json ]; then npm i; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i; \
  # Allow install without lockfile, so example works even without Node.js installed locally
  else echo "Warning: Lockfile not found. It is recommended to commit lockfiles to version control." && yarn install; \
  fi

COPY . .
RUN \
  if [ -f yarn.lock ]; then yarn build; \
  elif [ -f package-lock.json ]; then npm run build; \
  elif [ -f pnpm-lock.yaml ]; then pnpm build; \
  else yarn build; \
  fi

ENV PORT 5001
# Expose the port to the outside world
EXPOSE 5001
#EXPOSE ${PORT}
CMD \
  if [ -f yarn.lock ]; then yarn up; \
  elif [ -f package-lock.json ]; then npm run up; \
  elif [ -f pnpm-lock.yaml ]; then pnpm up; \
  else yarn up; \
  fi